import React, { Component } from 'react';
import { List } from 'antd';
import ItemFavorite from './item'

class FavoriteList extends Component {
    render() {
        return (
            <div>
                <List
                    grid={{ gutter: 16, colume: 4 }}
                    dataSource={this.props.items}
                    renderItem={item => {
                        <List.Item>
                            <ItemFavorite
                                item={item}
                                onItemMovieClick={this.onItemMovieClick} />
                        </List.Item>
                    }} />
            </div>
        );
    }
}

export default FavoriteList