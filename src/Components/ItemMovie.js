import React from 'react';
import { Card } from 'antd';
import TextTruncate from 'react-text-truncate';
import { connect } from 'react-redux';

const { Meta } = Card;

const mapDispatchToProps = dispatch => {
  return {
    // return dispatch() 
    // dispatch() send the 'item' to 'reducers'
    onItemMovieClick: item =>
      dispatch({
        type: 'click_item',
        payload: item
      })
  };
};

function ItemMovie(props) {
  const item = props.item;
  return (
    <Card
      onClick={() => {
        // get 'props' from Provider
        props.onItemMovieClick(item);
      }}
      hoverable
      cover={<img src={item.image_url} />}
    >
      <Meta
        title={item.title}
        description={
          <TextTruncate
            line={1}
            truncateText="…"
            text={item.overview}
            textTruncateChild={<a href="#">Read more</a>}
          />
        }
      />
    </Card>
  );
}

// connect() to connect to Provider
// return 'Component'
// TODO: required 'mapStateTpProps'
export default connect(
  null,
  mapDispatchToProps
)(ItemMovie);
